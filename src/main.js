//import VueKeycloakJs from '@dsb-norge/vue-keycloak-js'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';

//Vue.config.productionTip = false

/* Vue.use(VueKeycloakJs, {
  init: {
    // Use 'login-required' to always require authentication
    // If using 'login-required', there is no need for the router guards in router.js
    onLoad: 'check-sso'
  },
  config: {
    url: 'https://keycloak.califourchon.wtf/auth',
    clientId: 'lumen-front',
    realm: 'staging'
  },
  onReady: (keycloak) => {
    console.log(`I wonder what Keycloak returns: ${keycloak}`)
    console.log(keycloak.idToken)
    new Vue({
      router,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
}) */

new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')