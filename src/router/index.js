import Vue from 'vue'
import VueRouter from 'vue-router'
/*import TimelinePerso from '../views/TimelinePerso.vue'*/
import TimelineRandom from '../views/TimelineRandom.vue'
import UploadPhoto from '../views/UploadPhoto.vue'
import Profil from '../views/Profil.vue'
import NotFoundComponent from '../components/NotFoundComponent.vue'
import Classement from "../views/Classement";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/',
            name: 'timelinePerso',
            component: () =>
                import ('../views/TimelinePerso.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/timelineRandom',
            name: 'timelineRandom',
            component: TimelineRandom
        },
        {
            path: '/uploadPhoto',
            name: 'uploadPhoto',
            component: UploadPhoto
        },
        {
            path: '/profil',
            name: 'profil',
            component: Profil
        },
        {
            path: '/classement',
            name: 'classement',
            component: Classement
        },
        { path: '*', component: NotFoundComponent }
    ]
})

/*router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (router.app.$keycloak.authenticated) {
            next()
        } else {
            const loginUrl = router.app.$keycloak.createLoginUrl()
            window.location.replace(loginUrl)
        }
    } else {
        next()
    }
})*/

export default router